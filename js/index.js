var hasNativeEvents = NATIVE && NATIVE.plugins && NATIVE.plugins.sendRequest;

var pdfviewer = Class(function () {
	this.init = function () {
	}

	this.openFile = function(opts, cb){
		NATIVE.plugins.sendRequest("PDFViewerPlugin", "openFile", opts, function (err, res) {
			cb && cb(err, res);
		});
	};
});

exports = new pdfviewer();