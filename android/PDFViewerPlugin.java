package com.tealeaf.plugin.plugins;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.Build;
import java.util.HashMap;
import java.lang.Character;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

import com.tealeaf.plugin.IPlugin;
import com.tealeaf.plugin.PluginManager;
import com.tealeaf.LocalStorage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.app.Dialog;
import android.widget.TextView;
import android.widget.Button;
import android.widget.TableLayout.LayoutParams;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;


import com.tealeaf.EventQueue;
import com.tealeaf.event.*;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import android.app.ProgressDialog;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;
import android.webkit.URLUtil;
import com.joanzapata.pdfview.R;
import android.util.TypedValue;
import android.graphics.Color;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.EditText;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;



public class PDFViewerPlugin implements IPlugin {
	Activity context;
	Integer _requestId;
	String _fileKey;
    String _ads;
    int _adHeight;
	public PDFViewerPlugin() {
	}

	public void onCreateApplication(Context applicationContext) {
	}

	public void onCreate(Activity activity, Bundle savedInstanceState) {
		context = activity;
	}

	public static String generateMD5(String md5) {
	   try {
	        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
	        byte[] array = md.digest(md5.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < array.length; ++i) {
	          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
	       }
	        return sb.toString();
	    } catch (java.security.NoSuchAlgorithmException e) {
	    }
	    return null;
	}

	public void openFile(final String json, final Integer requestId) {
		_requestId = requestId;
		TeaLeaf.get().runOnUiThread(new Runnable() {
			public void run() {
				try {
					final JSONObject obj = new JSONObject(json);
                    _ads = obj.getString("ads");
                    _adHeight = obj.getInt("adHeight");
					final String file = obj.getString("file");
					final String fileKey = PDFViewerPlugin.generateMD5(file);
					_fileKey = fileKey;
					LocalStorage localStorage = TeaLeaf.get().getLocalStorage();
					String cacheFile = localStorage.getData(fileKey);
					DownloadAndOpenTask dl = new DownloadAndOpenTask();
					if(cacheFile != null && new File(cacheFile).exists()){
						dl.openFile(cacheFile);
					}else{
                        boolean status=false;
                        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = cm.getNetworkInfo(0);
                        if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                            status= true;
                        }else {
                            netInfo = cm.getNetworkInfo(1);
                            if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                                status= true;
                        }
                        if(status == false){
                            Toast.makeText(context, "Network error.", Toast.LENGTH_LONG).show();
                            PluginManager.sendResponse(null, "Network error", requestId);
                            return;
                        }
						URI uri = new URI(file);
						new DownloadAndOpenTask().execute(uri);
					}
				} catch(Exception e) {
					logger.log("{pdfviewer} Exception:", e.getMessage());
					PluginManager.sendResponse(null, e.getMessage(), requestId);
				}
			}
		});
	}

	private class DownloadAndOpenTask extends AsyncTask<URI, Integer, String> {

        // declare the dialog as a member field of your activity
        private ProgressDialog mProgressDialog;
        private File targetFile;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // instantiate it within the onCreate method
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    // cancel AsyncTask
                    cancel(false);
                }
            });
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(URI... fileUris) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection httpConnection = null;
            URI uri = fileUris[0];
            String targetFileName;
            int fileLength = 0;

            int sep = uri.toString().lastIndexOf("/");
            if (sep > 0) {
                targetFileName = uri.toString().substring(sep + 1, uri.toString().length());
            } else {
                targetFileName = uri.toString();
            }
            try {
                if (!uri.isAbsolute()){
                    // local file in assets folder
                    AssetManager am = context.getAssets();
                    input = am.open("www/" + uri.toString());

                } else if (uri.getScheme().equalsIgnoreCase("file")) {
                    // local file in phone storage
                    URLConnection urlConnection = uri.toURL().openConnection();
                    fileLength = urlConnection.getContentLength();
                    input = urlConnection.getInputStream();

                } else {
                    // Remote file
                    URL url = uri.toURL();
                    httpConnection = (HttpURLConnection) url.openConnection();
                    httpConnection.connect();

                    // expect HTTP 200 OK, so we don't mistakenly save error report
                    // instead of the file
                    if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        return "Server returned HTTP " + httpConnection.getResponseCode()
                                + " " + httpConnection.getResponseMessage();
                    }

                    // this will be useful to display download percentage
                    // might be -1: server did not report the length
                    fileLength = httpConnection.getContentLength();
                    input = httpConnection.getInputStream();
                }

                // download the file and save it in externalcachedir
                // so other apps can acces the file
                // targetFile = new File(context.getExternalCacheDir(), targetFileName);
                targetFile = new File(context.getCacheDir(), targetFileName);
                output = new FileOutputStream(targetFile);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()){
                    	logger.log("{pdfviewer} cancel downloading");
                    	PluginManager.sendResponse(new PDFViewerEvent(null, false), null, _requestId);
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (FileNotFoundException e) {
            	Toast.makeText(context, "File does not exists.", Toast.LENGTH_LONG).show();
                return null;
            } catch (Exception e) {
            	Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                return null;
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (httpConnection != null)
                    httpConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Download error! Please check your Network and try again.", Toast.LENGTH_LONG).show();
            } else {
            	logger.log("{pdfviewer} download successfully:",targetFile.getAbsolutePath());
            	LocalStorage localStorage = TeaLeaf.get().getLocalStorage();
            	localStorage.setData(_fileKey, targetFile.getAbsolutePath());
                openFile(targetFile.getAbsolutePath());
            }
        }

        public float dpFromPx(final float px) {
            return px / context.getResources().getDisplayMetrics().density;
        }

        public float pxFromDp(final float dp) {
            return dp * context.getResources().getDisplayMetrics().density;
        }

        public void openFile(String sUrl) {
        	//loading
        	final ProgressDialog loadingDialog = new ProgressDialog(context);
            loadingDialog.setMessage("Opening...");
            loadingDialog.setIndeterminate(false);
            loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loadingDialog.setCancelable(false);
            loadingDialog.show();

            File file = new File(sUrl);
            Uri uri = Uri.fromFile(file);
            String mimeType = URLConnection.guessContentTypeFromName(sUrl);
            String guessedFileName = URLUtil.guessFileName(sUrl, null, null);

            if (guessedFileName.contains(".pdf")) {
            	FrameLayout group = TeaLeaf.get().getGroup();
            	boolean hasAdView = false;
            	int adHeight = 0;
            	if(_ads != null && !_ads.equals("")){
            		hasAdView = true;
                    adHeight = (int) this.pxFromDp(_adHeight);
                    logger.log("{pdfviewer} adHeight", adHeight);
            	}
                // PDF file
                final PDFView pdfView = new PDFView(context,null);
				FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
					FrameLayout.LayoutParams.WRAP_CONTENT,
					hasAdView == false ? FrameLayout.LayoutParams.WRAP_CONTENT : context.getWindowManager().getDefaultDisplay().getHeight() - adHeight, 
					hasAdView && _ads.equals("bottom") ? Gravity.TOP | Gravity.CENTER_HORIZONTAL : Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL
				);
				group.addView(pdfView, layoutParams);
				//toolbar
				int buttonSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, context.getResources().getDisplayMetrics());
				final ImageButton backButton = new ImageButton(context);
                FrameLayout.LayoutParams backLayoutParams = new FrameLayout.LayoutParams(buttonSize, buttonSize, 
                    hasAdView == false || (hasAdView && _ads.equals("bottom")) ? Gravity.LEFT | Gravity.TOP:Gravity.LEFT | Gravity.BOTTOM
                );
				backButton.setImageResource(R.drawable.back_button);
				backButton.setAlpha(.5f);
				backButton.setBackgroundColor(Color.TRANSPARENT);
				backButton.setScaleType(ScaleType.FIT_CENTER);
                group.addView(backButton, backLayoutParams);

                int textLayoutSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, context.getResources().getDisplayMetrics());
                final TextView pageNumbers = new TextView(context);
                FrameLayout.LayoutParams textLayoutParams = new FrameLayout.LayoutParams(textLayoutSize * 2, textLayoutSize, 
                    hasAdView == false || (hasAdView && _ads.equals("bottom")) ? Gravity.RIGHT | Gravity.TOP : Gravity.RIGHT | Gravity.BOTTOM
                );
                int y = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                if(hasAdView && _ads.equals("top")) {
                    y = -y;
                }
                pageNumbers.setY(y);
                pageNumbers.setX(-(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, context.getResources().getDisplayMetrics()));
                pageNumbers.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                pageNumbers.setTextColor(Color.DKGRAY);
                // pageNumbers.setVisibility(View.GONE);
                // pageNumbers.setBackgroundColor(Color.BLACK);
                // pageNumbers.getBackground().setAlpha(128);
                group.addView(pageNumbers, textLayoutParams);

                backButton.setOnClickListener(new OnClickListener(){
                	@Override
                	public void onClick(View v){
                		logger.log("{pdfviewer} click back....");
                		TeaLeaf.get().getGroup().removeView(pdfView);
                		TeaLeaf.get().getGroup().removeView(backButton);
                		TeaLeaf.get().getGroup().removeView(pageNumbers);
                        EventQueue.pushEvent(new Event("pdfViewerExitEvent"));
                	}
                });

                pageNumbers.setOnClickListener(new OnClickListener(){
                	@Override
                	public void onClick(View v){
                		logger.log("{pdfviewer} click page....");
                		final EditText input = new EditText(context);
                		new AlertDialog.Builder(context)
						    .setTitle("Jump to")
						    // .setMessage(message)
						    .setView(input)
						    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						        public void onClick(DialogInterface dialog, int whichButton) {
						        	int value = 1;
						        	try{
						        		value = Integer.valueOf(input.getText().toString()); 
						        	}catch (NumberFormatException ex){
						        		value = 1;
						        	}
						            logger.log("{pdfviewer} jump to page:",value);
						            pdfView.jumpTo(value);
						        }
						    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						        public void onClick(DialogInterface dialog, int whichButton) {
						            // Do nothing.
						        }
						    }).show();
                	}
                });
                //open file...
				logger.log("{pdfviewer} openning file:", sUrl);
				final String _fileUrl = sUrl;
				pdfView.fromFile(file)
					    .defaultPage(1)
					    .showMinimap(false)
					    .enableSwipe(true)
					    // .onDraw(onDrawListener)
					    .onLoad(new OnLoadCompleteListener(){
					    	@Override
					    	public void loadComplete(int nbPages){
					    		// logger.log("{pdfviewer} loadComplete:", nbPages);
					    		loadingDialog.dismiss();
					    		PluginManager.sendResponse(new PDFViewerEvent(_fileUrl, true), null, _requestId);
					    	}
					    })
					    .onPageChange(new OnPageChangeListener(){
					    	@Override
					    	public void onPageChanged(int page, int pageCount){
					    		pageNumbers.setText(String.format("%s/%s", page, pageCount));
					    		EventQueue.pushEvent(new PDFViewerPageChangedEvent(page, pageCount, _fileUrl));
					    	}
					    })
					    .load();
            }
        }
    }

	public class PDFViewerEvent {
		String cacheFile;
		boolean completed;
		
		public PDFViewerEvent(String cacheFile, boolean completed) {
			this.completed = completed;
			this.cacheFile = cacheFile;
		}
	}

    public class PDFViewerPageChangedEvent extends Event {
        protected int page;
        protected int pageCount;
        protected String fileUrl;
        public PDFViewerPageChangedEvent(int page, int pageCount, String fileUrl){
            super("pdfViewerPageChanged");
            this.page = page;
            this.pageCount = pageCount;
            this.fileUrl = fileUrl;
        }
    }

	public void onResume() {
	}

	public void onStart() {
	}

	public void onPause() {
	}

	public void onStop() {
	}

	public void onDestroy() {
	}

	public void onNewIntent(Intent intent) {
	}

	public void setInstallReferrer(String referrer) {
	}

	public void onActivityResult(Integer request, Integer result, Intent data) {
	}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {
	}
}

