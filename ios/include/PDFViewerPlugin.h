#import "PluginManager.h"
#import <QuickLook/QuickLook.h>

@interface PDFViewerPlugin : GCPlugin <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (nonatomic, retain) TeaLeafViewController *tealeafViewController;
@property (nonatomic, retain) NSString *localFile;
@property (nonatomic, retain) QLPreviewController *previewController;
@property (nonatomic, retain) UIButton *doneButton;
@end
