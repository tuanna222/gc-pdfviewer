#import "PDFViewerPlugin.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <CommonCrypto/CommonDigest.h>
#import "platform/LocalStorage.h"

@interface PDFViewerPlugin ()
@end

@implementation PDFViewerPlugin
// The plugin must call super dealloc.
- (void) dealloc {
	[super dealloc];
}

// The plugin must call super init.
- (id) init {
	self = [super init];
	if (!self) {
		return nil;
	}
	return self;
}

- (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
	@try {
        self.tealeafViewController = appDelegate.tealeafViewController;
	}
	@catch (NSException *exception) {
		NSLog(@"{pdfviewer} error init: %@",exception);
	}
}

- (void) openFile:(NSDictionary *)jsonObject withRequestId:(NSNumber *)requestId {
    NSString *file = [jsonObject objectForKey:@"file"];
    NSString *_fileKey = [self md5:file];
    NSString *localFile = local_storage_get(_fileKey);
//    NSLog(@"localFile: %@",localFile);
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath: localFile];
//    NSLog(@"existed: %@", fileExists == true ? @"true":@"false");
    if (localFile && fileExists){
        [self openLocalFile:localFile jsonObject:jsonObject withRequestId:requestId];
    }else{
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo: self.tealeafViewController.view animated:YES];
        progress.mode = MBProgressHUDModeDeterminateHorizontalBar;
        progress.labelText = @"Downloading...";
        [self.tealeafViewController.view addSubview: progress];
        [progress setProgress: 0.0f ];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:file]];
        AFURLConnectionOperation *operation =   [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent: [file lastPathComponent]];
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long  totalBytesRead, long long totalBytesExpectedToRead) {
            //        progress.progress = (float)totalBytesRead / totalBytesExpectedToRead;
            [progress setProgress: (float)totalBytesRead / totalBytesExpectedToRead ];
            //        NSLog(@"{pdfviewer} download progress: %f", (float)totalBytesRead / totalBytesExpectedToRead);
        }];
        
        [operation setCompletionBlock:^{
//            NSLog(@"{pdfviewer} downloadComplete: %@", filePath);
            [progress hide: TRUE];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            local_storage_set(_fileKey, filePath);
            [self openLocalFile:filePath jsonObject:jsonObject withRequestId:requestId];
            
        }];
        [operation start];
    }
}

- (void) openLocalFile: (NSString*) filePath jsonObject: (NSDictionary *)jsonObject withRequestId:(NSNumber *)requestId {
    float frameWidth = self.tealeafViewController.view.frame.size.width;
    float frameHeight = self.tealeafViewController.view.frame.size.height;
    
    NSString *ads = [jsonObject objectForKey:@"ads"];
    NSInteger _adHeight = [[jsonObject objectForKey:@"adHeight"] integerValue];
    
    self.localFile = filePath;
    self.previewController = [[QLPreviewController alloc]init];
    self.previewController.delegate= self;
    self.previewController.dataSource= self;
    [self.previewController.navigationController setHidesBarsOnTap:YES];
    
    CGFloat adHeight = _adHeight;
    BOOL hasAds = false;
    if(ads && ![ads isEqualToString:@""]){
        hasAds = true;
        
        if([ads isEqualToString:@"bottom"]){
            self.previewController.view.frame = CGRectMake(0, 0, frameWidth, frameHeight - adHeight);
        }else{
            self.previewController.view.frame = CGRectMake(0, adHeight, frameWidth, frameHeight - adHeight);
        }
        
    }
    [self.tealeafViewController.view addSubview: self.previewController.view];
    
    UIFont *doneButtonFont = [UIFont systemFontOfSize: 15.0f];
    NSString *doneButtonText = @"DONE";
    CGSize doneButtonSize = [doneButtonText sizeWithFont:doneButtonFont];
    CGFloat doneButtonWidth = (doneButtonSize.width + 24.0f);
    
    
    
    self.doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.doneButton.frame = CGRectMake(frameWidth - doneButtonWidth - 8.0f, 8.0f + (hasAds && [ads isEqualToString:@"top"] ? adHeight : 0), doneButtonWidth, 30.0f);
    [self.doneButton setTitleColor:[UIColor colorWithWhite:0.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor colorWithWhite:1.0f alpha:1.0f] forState:UIControlStateHighlighted];
    [self.doneButton setTitle:doneButtonText forState:UIControlStateNormal];
    self.doneButton.titleLabel.font = doneButtonFont;
    [self.doneButton addTarget:self action:@selector(doneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.doneButton.autoresizingMask = UIViewAutoresizingNone;
    [[self.doneButton layer] setCornerRadius:8.0f];
    [[self.doneButton layer] setMasksToBounds:YES];
    [[self.doneButton layer] setBorderWidth:1.0f];
    [self.doneButton setAlpha:0.5];
    
    self.doneButton.backgroundColor = [UIColor grayColor];
    
    
    [self.tealeafViewController.view addSubview: self.doneButton];
    
    [[PluginManager get] dispatchJSResponse:nil withError: nil andRequestId:requestId];
}

- (void)doneButtonTapped:(id)sender {
    [self.doneButton removeFromSuperview];
    [self.previewController.view removeFromSuperview];
    [[PluginManager get] dispatchJSEvent:[NSDictionary dictionaryWithObjectsAndKeys:
                                          @"pdfViewerExitEvent",@"name", nil]];
    
}

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    return [NSURL fileURLWithPath: self.localFile];
}

- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG) strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

@end

